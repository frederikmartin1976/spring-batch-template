package net.bounceme.chronos.springbatch.tasklet;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.core.io.Resource;

/**
 * @author fxm105
 *
 */
public class ReadConfigFileTasklet implements Tasklet {
	
	private Resource resource;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		// TODO Auto-generated method stub
		return RepeatStatus.FINISHED;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}
}
