package net.bounceme.chronos.springbatch.tasklet;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import lombok.extern.slf4j.Slf4j;
import net.bounceme.chronos.springbatch.model.ConfigJobDTO;

/**
 * @author fxm105
 *
 */
@Slf4j
public class InitializeLoopTasklet implements Tasklet {

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		ConfigJobDTO config = (ConfigJobDTO) chunkContext.getStepContext().getJobExecutionContext().get("config");

		// Sets the first element
		config.setCurrentFile(config.getFiles().pop());

		log.debug("Current file is {}", config.getCurrentFile());

		return RepeatStatus.FINISHED;
	}

}
