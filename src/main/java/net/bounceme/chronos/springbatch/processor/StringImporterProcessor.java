package net.bounceme.chronos.springbatch.processor;

import org.springframework.batch.item.ItemProcessor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StringImporterProcessor implements ItemProcessor<String, String> {

    @Override
    public String process(String value) throws Exception {
    	log.debug("Process {}", value);
        return value;
    }
}
