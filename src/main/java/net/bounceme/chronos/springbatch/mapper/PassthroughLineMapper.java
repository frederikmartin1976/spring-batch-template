package net.bounceme.chronos.springbatch.mapper;

import org.springframework.batch.item.file.LineMapper;

public class PassthroughLineMapper implements LineMapper<String> {

	@Override
	public String mapLine(String line, int lineNumber) throws Exception {
		return line;
	}

}
