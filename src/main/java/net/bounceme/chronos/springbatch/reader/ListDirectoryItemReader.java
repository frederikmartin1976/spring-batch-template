package net.bounceme.chronos.springbatch.reader;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author federico
 *
 */
@Slf4j
public class ListDirectoryItemReader implements ItemReader<File> {
	
	protected List<File> files;
	
	/**
	 * @param directory
	 */
	public ListDirectoryItemReader(File directory) {
		if (Objects.isNull(directory)) {
			throw new IllegalArgumentException("The directory cannot be null");
		}
		
		if (!directory.isDirectory()) {
			throw new IllegalArgumentException("The specified file must be a directory");
		}
		
		files = Arrays.asList(directory.listFiles());
	}

	@Override
	public File read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		log.debug("Reading item");
		
		if (!files.isEmpty()) {
			return files.remove(0);
		}
		return null;
	}

}
