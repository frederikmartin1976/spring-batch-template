package net.bounceme.chronos.springbatch.support;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;

/**
 * Decorator con la configuración global de la tarea en ejecución. Sirve de
 * soporte de acceso a los contextos de ejecución de la tarea, por ejemplo
 * para acceder a las variables globales del trabajo en ejecución
 * 
 * @author fxm105
 *
 */
public abstract class EnvironmentStep {

	protected JobExecution jobExecution;
	
	protected ExecutionContext executionContext;
	
	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		jobExecution = stepExecution.getJobExecution();
		executionContext = jobExecution.getExecutionContext();
		
		before(stepExecution);
	}
	
	/**
	 * Para implementar en las clases heredadas
	 * 
	 * @param stepExecution
	 */
	protected abstract void before(StepExecution stepExecution);
}
