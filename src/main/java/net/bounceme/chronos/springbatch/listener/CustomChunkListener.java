package net.bounceme.chronos.springbatch.listener;

import org.springframework.batch.core.ChunkListener;
import org.springframework.batch.core.scope.context.ChunkContext;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomChunkListener implements ChunkListener {

	@Override
	public void beforeChunk(ChunkContext context) {
		log.info("Before chunk");
	}

	@Override
	public void afterChunk(ChunkContext context) {
		log.info("After chunk");
	}

	@Override
	public void afterChunkError(ChunkContext context) {
		log.info("After chunk: error");
	}

}
