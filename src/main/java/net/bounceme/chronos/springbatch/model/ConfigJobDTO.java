package net.bounceme.chronos.springbatch.model;

import java.io.Serializable;
import java.util.Stack;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Esta clase mantendrá en todo momento la configuración del trabajo
 * a lo largo de su ejecución. Algunas propiedades cambiarán su valor, 
 * por lo que este objeto podrá cambiar su estado conforme avance su ejecución
 * según se requiera.
 * 
 * @author fxm105
 *
 */
@Data @AllArgsConstructor @NoArgsConstructor
public class ConfigJobDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7744387155583287341L;

	private String configFileName;
	
	private String dirName;
	
	private String outputFileName;
	
	private Stack<String> files = new Stack<>();
	
	private String currentFile;

}
