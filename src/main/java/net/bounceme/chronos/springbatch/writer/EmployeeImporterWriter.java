package net.bounceme.chronos.springbatch.writer;

import java.util.List;

import org.springframework.batch.item.ItemWriter;

public class EmployeeImporterWriter implements ItemWriter<String> {

    @Override
    public void write(List<? extends String> items) throws Exception {
        for (String s : items) {
            System.out.println(s);
        }
    }

}
