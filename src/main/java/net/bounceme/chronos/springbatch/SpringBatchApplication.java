package net.bounceme.chronos.springbatch;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ImportResource;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class,BatchAutoConfiguration.class})
@ImportResource({
    "classpath:applicationContext.xml",
    "classpath:job.xml"
})
public class SpringBatchApplication implements ApplicationRunner {
	
	@Value("${file.name}")
	private String fileName;
	
	@Autowired
	private JobLauncher jobLauncher;
	
	@Autowired
	private Job job;

	public static void main(String[] args) {
		SpringApplication.run(SpringBatchApplication.class, args);
		
		// If completed, exit normally
		System.exit(0);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info("File name: {}", fileName);
		
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addString("filename", fileName);
		
		JobExecution result = jobLauncher.run(job, builder.toJobParameters());
		
		// Exit on failure
		if (ExitStatus.FAILED.equals(result.getExitStatus())) {
			System.exit(1);
		}
	}

}
