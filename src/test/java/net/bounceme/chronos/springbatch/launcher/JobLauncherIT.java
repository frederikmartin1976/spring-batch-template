package net.bounceme.chronos.springbatch.launcher;

import org.junit.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

import net.bounceme.chronos.springbatch.SpringBatchApplication;

@SpringBootTest(classes = SpringBatchApplication.class)
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class JobLauncherIT {
    
	@Test
	void contextLoads() {
		
	}
}
